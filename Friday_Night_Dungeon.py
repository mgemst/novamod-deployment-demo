from novamud import Dungeon, Room, Thing


##### When you´re home

class CarKeys(Thing):
    name = 'CarKeys'
    description = "You need to have the car keys before you can leave, cause the restaurante is too far"


class ApartmentRoom(Room):
    name = 'ApartmentRoom'
    description = ("Spartan but functional. Not particularly clean but that "
                   "can be taken care of before your parents come by for "
                   "your Sunday dinner that you normally hold at your house.")

    def init_room(self):
        self.add_thing(CarKeys())       

    def go_to(self, player, other_room_name):
        if not player.carrying or player.carrying.name != 'CarKeys':
            player.tell("You ain't going anywhere without those CarKeys!")
        else:
            super().go_to(player, other_room_name)


### then you go to have dinner

class TascaRoom(Room):
    name = 'TascaRoom'
    description = ("Your friendly local Tasca. The wine is cheap and passable "
                   "but certainly nothing special. The bitoque is fantasic.")

    def register_commands(self):
        return [
            'eat_bitoque',
            'drink_wine',
        ]

    def add_player(self, player):
        player.drink_level = 0
        super().add_player(player)

    def eat_bitoque(self, player):
        if player.drink_level > 0:
            player.drink_level -= 1
            player.tell(
                "Much better! Your drink level is now down to {}".format(
                    player.drink_level)
            )
        else:
            player.tell("mmmmmmmm, bitoque!")

    def eat_bitoque_describe(self):
        return "Have a bitoque in case you are getting a bit too tipsy."

    def drink_wine(self, player):
        player.drink_level += 1
        if player.drink_level == 1:
            player.tell('The price makes it taste okay!')
        elif 1 <= player.drink_level < 2:
            player.tell(
                "Your drink level is {}, you're still in the good zone".format(
                    player.drink_level
                )
            )
        elif player.drink_level >= 3:
            player.tell(
                "It might be good to have some food in order to bring that"
                "drink level down a bit... you're currently at drink "
                "level {}".format(player.drink_level)
            )

    def drink_wine_describe(self):
        return ("Have some wine! If you have a bit too much, you can always "
                "eat some food to bring yourself down")


#### you decide to hang out

class HotColumbians(Thing):
    name = 'HotColumbians'
    description = "You need to have the two Hot Columbians before you can leave to the hotel"

class TequillaShots(Thing):
    name = 'TequillaShots'
    description = "You can drink some tequilla shots in case you´re bored"



class ClubRoom(Room):
    name = 'ClubRoom'
    description = ("You can dance loads of reggeaton and end up with the two Hot columbians")

    def init_room(self):
        self.add_thing(HotColumbians())       
        self.add_thing(TequillaShots())     


    def register_commands(self):
        return [
            "Dance",
            "drink_TequillaShots" ]
        
    def add_player(self, player):
        player.drink_level = 0
        super().add_player(player)

    def drink_TequillaShots(self, player):
        player.drink_level += 1
        if player.drink_level == 1:
            player.tell('The price makes it taste okay!')
        elif 1 <= player.drink_level < 2:
            player.tell(
               "Your drink level is {}, you're still in the good zone".format(
                   player.drink_level
               )
           )
        elif player.drink_level >= 3:
            player.tell(
               "It might be good to dance a bit in order to get that"
               "drink level down a bit... you're currently at drink "
               "level {}".format(player.drink_level)
           )
   
    def drink_TequillaShots_describe(self):
       return ("You won´t go to a reggeaton party without some tequilla shots. If you have a bit too much, you can always "
               "dance a bit to get yourself down")

    def dance(self, player):
        if player.drink_level > 0:
            player.drink_level -= 1
            player.tell(
                "Much better! Your drink level is now down to {}".format(
                    player.drink_level)
            )
        else:
            player.tell("yyyyyyyyyyyyuuuuuuuuuuuuuuuuuuuuuuu")

    def Dance_describe(self):
        return "You didn´t come to the club just to be seated at the bar drinking tequilla shots"


    def go_to(self, player, other_room_name):
        if not player.carrying or player.carrying.name != 'HotColumbians':
            player.tell("You ain't going anywhere without those Hot Columbians on the dance floor!")
        else:
            super().go_to(player, other_room_name)


## you end up with a boy in a hotel


class HotelRoom(Room):
    name = 'HotelRoom'
    description = ("You ended up in the hotel next door with the two Hot Columbians you found at the party, here you will be able to get surprised with their capabilities")

    def init_room(self):
       self.add_thing(CarKeys())       

    def register_commands(self):
        return [
            "Surprise_me"]
 

    def Surprise_me (self, player):
        player.tell ("ooooooooohhhhhhh soooo gooooooddddd, I haven´t felt this feeling for a while . these shoes are indeed really confortable. this Hot Columbians Brand  is definetely my favourite!")


    def Surprise_me_describe(self):
        return "Let the Hot Columbians show what they´re capable of"       


##### The Dungeon that structures the all game

class Friday_Night_Dungeon(Dungeon):

    name = 'Friday_Night_Dungeon'
    description = ("It´s Friday night and you´re bored at home. You are now a young adult. You have a crappy apartment"
                   " and a car with which you can go to meet up with your "
                   "friends at a TascaRoom. And eventually head to one of the clubs around wondering to what the night can offer your today")

    def init_dungeon(self):
        ar = ApartmentRoom(self)
        tr = TascaRoom(self)
        cr = ClubRoom (self)
        hr = HotelRoom (self)
        ar.connect_room(tr, two_way=True)
        tr.connect_room(cr, two_way=True)
        cr.connect_room(hr, two_way=True)
        self.start_room = ar


if __name__ == '__main__':
    Friday_Night_Dungeon().start_dungeon()

if __name__ == '__main__':
    import sys
    host, port = None, None
    if len(sys.argv) == 2:
        host, port = sys.argv[1].split(':')
    Friday_Night_Dungeon(host, port).start_dungeon()
